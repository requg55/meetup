package database

import java.sql.Timestamp

import models.Token
import slick.jdbc.PostgresProfile.api._

final class TokenTable(tag: Tag) extends Table[Token](tag, "token") {
    def id = column[String]("id", O.PrimaryKey)
    def userId = column[String]("userId", O.Unique)
    def created = column[Timestamp]("created")

    def * = (id, userId, created).mapTo[Token]
}
