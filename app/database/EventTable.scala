package database

import slick.jdbc.PostgresProfile.api._
import models.Event

final class EventTable(tag: Tag) extends Table[Event](tag, "event"){
    def id = column[String]("id", O.PrimaryKey)
    def ownerId = column[String]("ownerId")
    def name = column[String]("name")
    def description = column[String]("description")
    def lat = column[String]("lat")
    def lng = column[String]("lng")

    def * = (id, ownerId, name, description, lat, lng).mapTo[Event]
}