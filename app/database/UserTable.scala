package database
import models.User
import slick.jdbc.PostgresProfile.api._

final class UserTable(tag: Tag) extends Table[User](tag, "user") {
    def id = column[String]("id", O.PrimaryKey)
    def login = column[String]("login", O.Unique)
    def password = column[String]("password")

    def * = (id, login, password).mapTo[User]
}