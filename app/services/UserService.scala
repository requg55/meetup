package services

import database.{TokenTable, UserTable}
import javax.inject.{Inject, Singleton}
import models.{Token, User}
import org.postgresql.util.PSQLException
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.PostgresProfile
import slick.lifted
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success
import scala.util.Failure
import scala.concurrent.Future

@Singleton
class UserService @Inject() (val dbConfigProvider: DatabaseConfigProvider) extends HasDatabaseConfigProvider[PostgresProfile]{
    import profile.api._

    private lazy val users = lifted.TableQuery[UserTable]
    private lazy val tokens = lifted.TableQuery[TokenTable]

    def register(user: User): Future[Either[Error, Token]] = add(user).flatMap {
        case Right(u) => createToken(u).map(t => Right(t))
        case Left(error) => Future.successful(Left(error))
    }

    def login(login: String, password: String): Future[Either[Error, (User, Token)]] = {
        db.run(users.filter(u => u.login === login && u.password === password).result.headOption) flatMap {
            case Some(u) => for {token <- newToken(u) } yield Right((u: User, token))
            case None => Future.successful(Left(Error("notFound")))
        }
    }

    def findByToken(token: String): Future[Option[User]] = {
        val q = for {
            (token, user) <- tokens join users on((t, u) => t.id === token && u.id === t.userId)
        } yield user

        db.run(q.result.headOption)
    }

    private def add(user: User): Future[Either[Error, User]] = {
        db.run((users += user).asTry).map {
            case Success(_) => Right(user)
            case Failure(err: PSQLException) => Left(Error(err.getMessage))
        }
    }

    private def createToken(user: User) = {
        val token = Token(userId = user.id)
        db.run(tokens += token).map(_ => token)
    }

    private def isTokenAlive(user: User) = {
        val token = db.run(tokens.filter(_.userId === user.id).result.headOption)
        token.map(t => t.exists(_.isExpired))
    }

    private def deleteToken(user: User) = {
        db.run(tokens filter(_.userId === user.id) delete)
    }

    private def newToken(user: User) = {
        val token = Token(userId = user.id)
        val q = tokens.insertOrUpdate(token)
        db.run(q).map(_ => token)
    }

}

case class Error(msg: String)