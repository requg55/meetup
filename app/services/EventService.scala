package services

import database.EventTable
import javax.inject.{Inject, Singleton}
import models.{Event, User}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.PostgresProfile
import slick.lifted
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class EventService@Inject() (val dbConfigProvider: DatabaseConfigProvider) extends HasDatabaseConfigProvider[PostgresProfile]{
    import profile.api._

    private val events = lifted.TableQuery[EventTable]

    def list(user: User): Future[Seq[Event]] = {
        db.run(events.filter(_.ownerId === user.id).result)
    }

    def add(event: Event): Future[Event] = {
        db run (events += event) map (_ => event)
    }

    def delete(event: Event) = {
        db.run(events.filter(_.id === event.id).delete)
    }

    def update(event: Event): Future[Event] = {
        db.run(events.update(event)) map (_ => event)
    }
}
