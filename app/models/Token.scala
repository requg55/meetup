package models

import java.sql.Timestamp
import java.util.{Date, UUID}

case class Token(id: String = UUID.randomUUID().toString, userId: String, created: Timestamp = new Timestamp(new Date().getTime)) {
    def isExpired: Boolean = new Timestamp(created.getTime + (1000 * 60 * 60)) after new Timestamp(new Date().getTime)
}
