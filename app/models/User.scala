package models

import java.util.UUID

final case class User(id: String = UUID.randomUUID().toString, login: String, password: String)