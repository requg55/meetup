package models

import java.util.UUID

case class Event(id: String = UUID.randomUUID().toString,
                 ownerId: String,
                 name: String,
                 description: String,
                 lat: String,
                 lng: String)
