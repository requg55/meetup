package controllers

import javax.inject.{Inject, Singleton}
import models.User
import play.api.mvc._
import services.UserService
import play.api.mvc.Results._
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AuthAction @Inject()(val parser: BodyParsers.Default, userService: UserService)(implicit val executionContext: ExecutionContext)
    extends ActionBuilder[UserRequest, AnyContent] with ActionRefiner[Request, UserRequest] {

    def refine[A](request: Request[A]): Future[Either[Result, UserRequest[A]]] = {
        request.session.get("token") match {
            case Some(token) => userService.findByToken(token).map {
                case Some(user) => Right(new UserRequest(user, request))
                case None => Left(Unauthorized)
            }
            case None => Future.successful(Left(Unauthorized))
        }
    }
}

class UserRequest[A](val user: User, request: Request[A]) extends WrappedRequest[A](request)