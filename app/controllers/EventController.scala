package controllers

import javax.inject.{Inject, Singleton}
import play.api.mvc.{AbstractController, ControllerComponents}
import services.{EventService, UserService}
import scala.concurrent.ExecutionContext

@Singleton
class EventController @Inject()(authAction: AuthAction,
                                cc: ControllerComponents,
                                eventService: EventService,
                                val userService: UserService)(implicit ec: ExecutionContext)
    extends AbstractController(cc) with play.api.i18n.I18nSupport {

    def eventList = authAction.async { r =>
        eventService.list(r.user).map(l => Ok(views.html.eventList(l)))
    }
}