package controllers

import javax.inject._
import play.api.mvc._
import services.{Error, UserService}
import play.api.data._
import play.api.data.Forms._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class HomeController @Inject()(authAction: AuthAction,
                               cc: ControllerComponents,
                               val userService: UserService)(implicit ec: ExecutionContext)
    extends AbstractController(cc) with play.api.i18n.I18nSupport {

    def index = authAction { r =>
        Ok(views.html.map())
    }

    def login = Action { implicit request =>
        Ok(views.html.auth(authForm))
    }

    def loginHandler = Action.async {implicit request =>
        authForm.bindFromRequest.fold(
            _ => Future.successful(Ok(views.html.auth(authForm, "Form validation error"))),
            userData =>
                userService.login(userData.login, userData.password) map {
                    case Right((user, token)) => Redirect(routes.EventController.eventList())
                        .withSession("token" -> token.id)
                    case Left(Error(msg)) => Ok(views.html.auth(authForm, msg))
                }
        )
    }


    val authForm = Form(
        mapping(
            "login" -> email,
            "password" -> nonEmptyText(minLength = 6, maxLength = 20)
        )(UserData.apply)(UserData.unapply)
    )
}

case class UserData(login: String, password: String)




