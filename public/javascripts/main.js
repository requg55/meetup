// SIDEBAR
$(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });

// MAP
ymaps.ready(init);
function init(){
    var myMap = new ymaps.Map("map", {
        center: [53.395348, 58.997115],
        zoom: 13
    });

    var myPlacemark = new ymaps.Placemark([53.395348, 58.997115], {
        hintContent: 'Содержимое всплывающей подсказки',
        balloonContent: 'Содержимое балуна'
    });

    myMap.geoObjects.add(myPlacemark);
}