# Users schema

# --- !Ups

CREATE TABLE "user"
(
    "id" varchar not null primary key,
    "login" varchar not null,
    "password" varchar not null
);

CREATE TABLE "token"(
    "id" varchar not null primary key,
    "userId" varchar not null,
    "created" timestamp not null
);

CREATE TABLE "event"(
    "id" varchar not null primary key,
    "ownerId" varchar not null references "user"("id"),
    "name" varchar not null,
    "description" varchar not null,
    "lat" varchar,
    "lng" varchar
);

INSERT INTO "user" values('1', 'admin@admin.ru', '1234567');
INSERT INTO "event" values('1', '1', 'event_name', 'event_desc', '53.395348', '58.997115');
# --- !Downs
DROP TABLE IF EXISTS "user" CASCADE;
DROP TABLE IF EXISTS "token";